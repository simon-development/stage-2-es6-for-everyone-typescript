import { showModal } from '../modal/modal';
import { createFighterImage } from '../fighterPreview';
import { IFighter } from '../../interfaces/fighter';

export function showWinnerModal(fighter: IFighter): void {
  const params = {
    title: fighter.name,
    bodyElement: createFighterImage(fighter),
    onClose: () => { window.location.reload(true)}
  }
  showModal(params);
}

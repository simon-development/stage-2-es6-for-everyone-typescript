import { createElement } from '../helpers/domHelper';
import { IFighter } from '../interfaces/fighter';

export function createFighterPreview(fighter: IFighter | undefined, position: "left" | "right"): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    fighterElement.append(createFighterImage(fighter));

    const fighterInfo = createElement({
      tagName: 'div',
      className: `fighter-info`
    });
    fighterElement.append(fighterInfo);

    const fighterName = createElement({
      tagName: 'p',
      className: ''
    });
    fighterName.innerText = `${fighter.name}`;
    fighterInfo.append(fighterName);

    const fighterHealth = createElement({
      tagName: 'p',
      className: ''
    });
    fighterHealth.innerText = `Health: ${fighter.health}`;
    fighterInfo.append(fighterHealth);

    const fighterAttack = createElement({
      tagName: 'p',
      className: ''
    });
    fighterAttack.innerText = `Attack: ${fighter.attack}`;
    fighterInfo.append(fighterAttack);

    const fighterDefense = createElement({
      tagName: 'p',
      className: ''
    });
    fighterDefense.innerText = `Defense: ${fighter.defense}`;
    fighterInfo.append(fighterDefense);
  }
  return fighterElement;
}

export function createFighterImage(fighter: IFighter): HTMLElement {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

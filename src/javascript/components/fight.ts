import { controls } from '../../constants/controls';
import { IFighter } from '../interfaces/fighter';


export async function fight(firstFighter: IFighter, secondFighter: IFighter): Promise<IFighter> {
  firstFighter.healthLeft = firstFighter.health;
  secondFighter.healthLeft = secondFighter.health;
  let pressed = new Set();
  let firstFighterLastCriticalUsageDate = Date.now();
  let secondFighterLastCriticalUsageDate = Date.now();

  function checkCritical(): void {
    let firstPressedCritical = true;
    let secondPressedCritical = true;
    for (let code of controls.PlayerOneCriticalHitCombination) {
      if (!pressed.has(code)) {
        firstPressedCritical = false;
      }
    }
    for (let code of controls.PlayerTwoCriticalHitCombination) {
      if (!pressed.has(code)) {
        secondPressedCritical = false;
      }
    }

    if (firstPressedCritical && (Date.now() - firstFighterLastCriticalUsageDate >= 10000)) {
      const demage = getCriticalHitPower(firstFighter);
      secondFighter.healthLeft = Math.max(0, secondFighter.healthLeft! - demage);
      firstFighterLastCriticalUsageDate = Date.now();
    }
    if (secondPressedCritical && (Date.now()- secondFighterLastCriticalUsageDate >= 10000)) {
      const demage = getCriticalHitPower(secondFighter);
      firstFighter.healthLeft = Math.max(0, firstFighter.healthLeft! - demage);
      secondFighterLastCriticalUsageDate = Date.now();
    }
    updateHealth(firstFighter, secondFighter);
  }
  
  const weHaveAWinnerPromise = new Promise<IFighter>((resolve) => {
    // resolve the promise with the winner when fight is over
    document.addEventListener('keydown', function(event) {
      pressed.add(event.code);
      checkCritical();
    });
  
    document.addEventListener('keyup', function(event) {
      pressed.delete(event.code);
      let code = event.code;
      if (code === controls.PlayerOneAttack && !pressed.has(controls.PlayerOneBlock)) {
        const demage = pressed.has(controls.PlayerTwoBlock) ? getDamage(firstFighter, secondFighter) : getHitPower(firstFighter);
        secondFighter.healthLeft = Math.max(0, secondFighter.healthLeft! - demage);
        updateHealth(firstFighter, secondFighter);
      }
      if (code === controls.PlayerTwoAttack && !pressed.has(controls.PlayerTwoBlock)) {
        const demage = pressed.has(controls.PlayerOneBlock) ? getDamage(secondFighter, firstFighter) : getHitPower(secondFighter);
        firstFighter.healthLeft = Math.max(0, firstFighter.healthLeft! - demage);
        updateHealth(firstFighter, secondFighter);
      }
      if (firstFighter.healthLeft === 0 || secondFighter.healthLeft === 0) {
        let winner = firstFighter.healthLeft === 0 ? secondFighter : firstFighter;
        resolve(winner);
      }    
    });
  });

  return weHaveAWinnerPromise;
}

export function getDamage(attacker: IFighter, defender: IFighter): number {
  return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
  // return damage
}

export function getHitPower(fighter: IFighter): number {
  return fighter.attack! * (Math.random() + 1);
  // return hit power
}

export function getCriticalHitPower(fighter:IFighter): number {
  return fighter.attack! * 2;
}

export function getBlockPower(fighter:IFighter): number {
  return fighter.defense! * (Math.random() + 1)
  // return block power
}

function updateHealth(firstFighter: IFighter, secondFighter: IFighter) {
  const firstFighterIndicator = document.getElementById('left-fighter-indicator')!;
  const secondFighterIndicator = document.getElementById('right-fighter-indicator')!;
  firstFighterIndicator.style.width = firstFighter.healthLeft! / firstFighter.health! * 100 + '%';
  secondFighterIndicator.style.width = secondFighter.healthLeft! / secondFighter.health! * 100 + '%';
}
export interface IFighter {
  _id: string,
  name: string,
  source: string,
  attack?: number,
  health?: number,
  defense?: number,
  healthLeft?: number
}